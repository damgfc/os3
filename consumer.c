/*
$Author: o-moring $
$Log: consumer.c,v $
Revision 1.5  2015/03/12 12:53:43  o-moring
final

Revision 1.4  2015/03/11 23:24:07  o-moring
shared memory

Revision 1.3  2015/03/09 04:25:15  o-moring
shared memory

$Date: 2015/03/12 12:53:43 $
*/
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/shm.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include "shared.h"

sharedStruct *shmP;
int count;
char filename[8];

int main(int argc, char **argv){
	printf("BEGIN CONSUMER\n--------------------\n");

	/** Register the alarms to catch **/
	signal(SIGALRM, signalHandling);
	signal(SIGINT, signalHandling);

	/** Make sure consumer is being called with a argument **/
	if (argc != 2){
		perror("Improper consumer execution\nExpected `consumer [n]'");
		exit(1);
	} 

	count = atoi(argv[1]);
	sprintf(filename, "cons%02d.log", count);
	writeToLog(filename, "STARTED");

	/** Get shared memory access **/
	int shmid = shmget ( SHM_KEY, sizeof(sharedStruct), 0777);

	if (shmid == -1){
		perror("Error in child shmget");
		exit(1);
	}
	shmP = (sharedStruct *)(shmat(shmid, 0, 0));
	criticalSection();
	printf("--------------------\nEND CONSUMER\n");
	return 1;
}

criticalSection(){
	int i;
	for (i = 0; i < shmP->bufferFlagSize; i++){
		if (shmP->bufferFullFlag[i]){ 		// look for full flag set on
			printf("Consumer %d: %s\n", count, shmP->buffer[i]); 
		}
		// write to log
		writeToLog(filename, shmP->buffer[i]);
		shmP->bufferFullFlag[i] = 0; // turn off full flag
	}

}

remainderSection(){
	//sleeps for random time
}

