CC = gcc
CFLAGS = 
OBJSM = master.o shared.o
OBJSP = producer.o shared.o 
OBJSC = consumer.o shared.o
PROGM = master 
PROGP = producer
PROGC = consumer 

.SUFFIXES: .c .o

all: $(PROGM) $(PROGP) $(PROGC)

$(PROGM): $(OBJSM)
	$(CC) -g -o $@ $(OBJSM)

$(PROGP): $(OBJSP)
	$(CC) -g -o $@ $(OBJSP)

$(PROGC): $(OBJSC)
	$(CC) -g -o $@ $(OBJSC)

shared.o: shared.c
	$(CC) -g -c shared.c
.c.o:
	$(CC) -c -o $@ $<
	
clean:
	rm *.o 
	rm $(PROGM) $(PROGP) $(PROGC) 
