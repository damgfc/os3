/*
$Author: o-moring $
$Log: producer.c,v $
Revision 1.5  2015/03/12 12:53:43  o-moring
final

Revision 1.4  2015/03/11 23:24:07  o-moring
shared memory

Revision 1.3  2015/03/09 04:25:15  o-moring
shared memory

$Date: 2015/03/12 12:53:43 $
*/
#include <stdio.h>
#include <sys/shm.h>
#include "producer.h"
#include "shared.h"

sharedStruct *shmP;
char filename[15] = "prod.log";

int main(int argc, char **argv){
	char string[250];

	printf("BEGIN PRODUCER\n-------------\n");
    
    /** Register the alarms to catch **/
    signal(SIGALRM, signalHandling);
    signal(SIGINT, signalHandling);
	
	writeToLog(filename, "STARTED");  // Write to log that it started
	
	/** Create shared memory **/
	int shmid = shmget(SHM_KEY, sizeof(sharedStruct), 0777);

	if (shmid == -1){
		perror("Unsuccessful shmget");
		exit(1);
	}
	shmP = (sharedStruct *)(shmat(shmid, 0, 0));

	while(1) {
		readFromFile();
	}
	
	printf("-------------\nEND PRODUCER\n");
	return 1;
}

void readFromFile(){
	FILE *inputFP;
	int i;
	char message[250];
	inputFP = fopen("input.txt", "r");

	if (inputFP == NULL){
		perror("Can't open input file");
	}
	if (!shmP->eofFlag){
		for (i = 0; i < shmP->bufferFlagSize; i++){
			if (!shmP->bufferFullFlag[i]){
				if (fgets(shmP->buffer[i], sizeof(shmP->buffer[i]), inputFP) != NULL){
					shmP->bufferFullFlag[i] = 1;
					sprintf(message, "WRITE %d %s", i+1, shmP->buffer[i]);
					writeToLog(filename, message);
				}
				else {
					shmP->eofFlag = 1;
				}
			}
		}
	}
	
	fclose(inputFP);
}
