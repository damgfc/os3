/*
$Author: o-moring $
$Log: master.c,v $
Revision 1.5  2015/03/12 12:53:43  o-moring
final

Revision 1.4  2015/03/11 23:24:07  o-moring
shared memory

Revision 1.3  2015/03/09 04:25:15  o-moring
shared memory

Revision 1.2  2015/03/05 01:28:51  o-moring
initial

$Date: 2015/03/12 12:53:43 $
*/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include "shared.h"

sharedStruct *shmP;
int pidArray[21];
static int k = 0; // counter for the pidArray

int main(int argc, char **argv){
	char filename[15] = "main.log";
	char testString[20] = "newString";
	int shmid, i, j;
	char consumerArg[3];
	pid_t child;
	
	removeLogFiles(); // Remove log files
		
	/** Register the alarms to catch **/
	signal(SIGALRM, signalHandling);
	signal(SIGINT, signalHandling);
	
	/** Create Shared Memory and Attach **/
	shmid = shmget(SHM_KEY, sizeof(sharedStruct), IPC_CREAT | 0777);
	
	if (shmid == -1){
		perror("Unsuccessful shmget");
		exit(1);
	}
	else {
		shmP = (sharedStruct *)(shmat(shmid, 0, 0));
		if (shmP == (void *)-1) {
			return -1;
		}
		
		/** Get array sizes and set values to 0 **/
		shmP->bufferFlagSize = sizeof(shmP->bufferFullFlag)/(sizeof(int));
		shmP->stateSize = sizeof(shmP->state)/(sizeof(int));
		
		for (j = 0; j < shmP->bufferFlagSize; j++){
			shmP->bufferFullFlag[j] = 0;
		}
		
		for (j = 0; j < shmP->stateSize; j++){
			shmP->state[j] = 0;
		}		
		
		/** Set Number of Consumers **/
		if (argc != 2){
			shmP->numberOfConsumers = 10;
		}
		else {
			if (atoi(argv[1]) < 1 || atoi(argv[1]) > 18) {
				shmP->numberOfConsumers = 10;
			}
			else {
				shmP->numberOfConsumers = atoi(argv[1]);
			}
		}
	}

	alarm(100); // Start the alarm

	writeToLog(filename, testString); // Create the main log
	
	/** Go through loop to create 1 producer and n consumers **/
	for (i = 0; i < (shmP->numberOfConsumers + 1); i++) {
		child = fork();
		sleep(1);
		switch(child) {
			case -1:
					perror("Unable to fork\n");
					exit(1);
			case 0:
					if (i == 0) {
						//printf("child: %d\n", getpid());
						execl("./producer", "producer", NULL); //Exec to producer
					}
					else {
						//printf("child: %d\n", getpid());						
						sprintf(consumerArg, "%d", i);
						execl("./consumer", "consumer", consumerArg, NULL);
					}
					break;
			default:
					pidArray[k] = child; //Add pid to array and increment
					k++;										
					break;
		}
	}
	return 1;
}

/***
pidArray[0] = Producer 
pidArray[1] = Consumer01
pidArray[2] = Consumer02
pidArray[N] = Master
***/


