#ifndef SHARED_H
#define SHARED_H

#include <time.h>
#include <signal.h>
#include <stdlib.h>
#define SHM_KEY 4255

	/***************/
	/* idle = 0    */
	/* want_in = 1 */
	/* in_cs = 2   */
	/***************/
enum state { idle, want_in, in_cs };


typedef struct {
	int bufferFullFlag[5];
	char buffer[5][128];
	int state[18]; // array of 18 state - each consumer needs state
	int turn; 
	int numberOfConsumers;
	int stateSize;
	int bufferFlagSize;
	int eofFlag;
	int termFlags[10]; // EOF, DIE, Don't Die
	int pidArray[21];
} sharedStruct;

void writeToLog(char *filename, char *string);
char *getCurrentTime();
void signalHandling(int sigNum, int array[], int size);
void detachAndRemove(int shmid, void *shmaddr);
void removeLogFiles();
#endif
